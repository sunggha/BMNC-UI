"""UI_BMNC URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.11/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf.urls import url, include
from django.contrib import admin
import berita.urls as tambah_berita
import polling.urls as tambah_polling

urlpatterns = [
    url(r'^admin/', admin.site.urls),
	url(r'^tambah_berita/', include((tambah_berita,'tambah_berita'),namespace='tambah_berita')),
	url(r'^tambah_polling/', include((tambah_polling,'tambah_polling'),namespace='tambah_polling')),
]
